package com.channat.productdemo.controller;

import com.channat.productdemo.model.Product;
import com.channat.productdemo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/index")
    public String getProductList(ModelMap modelMap) {
        List<Product> products = productService.products("Drink");
        modelMap.addAttribute("products", products);
        System.out.println(products);
        return "index";

    }


}
