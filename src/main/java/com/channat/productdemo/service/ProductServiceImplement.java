package com.channat.productdemo.service;

import com.channat.productdemo.model.Product;
import com.channat.productdemo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImplement  implements  ProductService{

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> products(String productName) {
        return productRepository.products(productName);
    }
}
