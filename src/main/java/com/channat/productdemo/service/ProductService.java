package com.channat.productdemo.service;

import com.channat.productdemo.model.Product;

import java.util.List;

public interface ProductService {

    List<Product> products(String productName);

}
