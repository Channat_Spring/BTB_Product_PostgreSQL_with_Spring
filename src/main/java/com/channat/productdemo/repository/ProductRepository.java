package com.channat.productdemo.repository;

import com.channat.productdemo.model.Product;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository {


    @Select("SELECT * " +
            "FROM func_get_product_by_category(#{categoryName}) " +
            "LIMIT #{limit}")
    @Results({
            @Result(column = "r_id", property = "id"),
            @Result(column = "r_product_name", property = "name"),
            @Result(column = "r_category_name", property = "category.name")
    })
    List<Product> products(String categoryName);


}
